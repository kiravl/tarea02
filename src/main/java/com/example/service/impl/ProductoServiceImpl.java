package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Producto;
import com.example.repository.ProductoRepository;
import com.example.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {
	
	@Autowired
	private ProductoRepository repository;

	@Override
	public Producto registrar(Producto consulta) {
		return this.repository.save(consulta);
	}

	@Override
	public void modificar(Producto consulta) {
		this.repository.save(consulta);
	}

	@Override
	public void eliminar(Integer idConsulta) {
		this.repository.delete(idConsulta);
	}

	@Override
	public Producto listarId(Integer idConsulta) {
		return this.repository.findOne(idConsulta);
	}

	@Override
	public List<Producto> listar() {
		return this.repository.findAll();
	}

}
