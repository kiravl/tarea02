package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Persona;
import com.example.repository.PersonaRepository;
import com.example.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {
	
	@Autowired
	private PersonaRepository repository;

	@Override
	public Persona registrar(Persona consulta) {
		return this.repository.save(consulta);
	}

	@Override
	public void modificar(Persona consulta) {
		this.repository.save(consulta);
	}

	@Override
	public void eliminar(Integer idConsulta) {
		this.repository.delete(idConsulta);
	}

	@Override
	public Persona listarId(Integer idConsulta) {
		return this.repository.findOne(idConsulta);
	}

	@Override
	public List<Persona> listar() {
		return this.repository.findAll();
	}

}
