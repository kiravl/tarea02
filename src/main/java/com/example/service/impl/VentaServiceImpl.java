package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Venta;
import com.example.repository.VentaRepository;
import com.example.service.VentaService;

@Service
public class VentaServiceImpl implements VentaService {
	
	@Autowired
	private VentaRepository repository;

	@Override
	public Venta registrar(Venta venta) {
		venta.getDetalles().forEach(x -> x.setVenta(venta));
		return this.repository.save(venta);
	}

	@Override
	public void modificar(Venta venta) {
		this.repository.save(venta);
	}

	@Override
	public void eliminar(Integer idConsulta) {
		// TODO Auto-generated method stub

	}

	@Override
	public Venta listarId(Integer idConsulta) {
		return this.repository.findOne(idConsulta);
	}

	@Override
	public List<Venta> listar() {
		return this.repository.findAll();
	}

}
