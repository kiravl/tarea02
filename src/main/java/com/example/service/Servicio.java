package com.example.service;

import java.util.List;

public interface Servicio<T, t> {
	
	T registrar(T consulta);

	void modificar(T consulta);

	void eliminar(t idConsulta);

	T listarId(t idConsulta);

	List<T> listar();

}